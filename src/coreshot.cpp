/*
    *
    * This file is a part of CoreShot.
    * A screen capture utility for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include <QFile>
#include <QTimer>
#include <QDateTime>
#include <QFileDialog>
#include <QCloseEvent>
#include <QClipboard>
#include <QMimeData>
#include <QScreen>

#include <cprime/cprime.h>
#include <cprime/appopenfunc.h>
#include <cprime/shareit.h>
#include <cprime/messageengine.h>

#include "coreshotdialog.h"
#include "coreshot.h"
#include "ui_coreshot.h"


coreshot::coreshot(QWidget *parent) : QWidget(parent)
  , ui(new Ui::coreshot)
  , smi(new settings)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);

    loadSettings();
    startSetup();

    files = "";
}

coreshot::~coreshot()
{
	delete smi;
    delete ui;
}
/**
 * @brief Setup ui elements
 */
void coreshot::startSetup()
{
    // set window size
    this->resize(800, 500);

    if (uiMode == 2) {
        // setup mobile UI
        this->setWindowState(Qt::WindowMaximized);
        setupIcons();
        // all toolbuttons icon size in toolBar
        QList<QToolButton *> toolBtns2 = this->findChildren<QToolButton *>();

        Q_FOREACH (QToolButton *b, toolBtns2) {
            if (b) {
                b->setIconSize(toolsIconSize);
                b->setToolButtonStyle(Qt::ToolButtonIconOnly);
            }
        }
    } else {
        // setup desktop or tablet UI

        if(windowMaximized){
            this->setWindowState(Qt::WindowMaximized);
        } else{
            this->resize(windowSize);
        }
    }
}

void coreshot::setupIcons()
{
    ui->newShot->setIcon(CPrime::ThemeFunc::themeIcon( "folder-pictures-symbolic", "bitmap-trace", "bitmap-trace" ));
    ui->shareIt->setIcon(CPrime::ThemeFunc::themeIcon( "document-send-symbolic", "document-send-symbolic", "document-send" ));
    ui->openInEditor->setIcon(CPrime::ThemeFunc::themeIcon( "document-edit-symbolic", "edit-rename", "document-edit" ));
    ui->save->setIcon(CPrime::ThemeFunc::themeIcon( "document-save-symbolic", "document-save", "document-save" ));
    ui->saveAs->setIcon(CPrime::ThemeFunc::themeIcon( "document-save-as-symbolic", "document-save-as", "document-save-as" ));
    ui->saveAs->setIcon(CPrime::ThemeFunc::themeIcon( "document-save-as-symbolic", "document-save-as", "document-save-as" ));
    ui->cancel->setIcon(CPrime::ThemeFunc::themeIcon( "edit-delete-symbolic", "edit-delete", "edit-delete" ));
}

/**
 * @brief Loads application settings
 */
void coreshot::loadSettings()
{
    // get CSuite's settings
    uiMode = smi->getValue("CoreApps", "UIMode");
    listViewIconSize = smi->getValue("CoreApps", "ListViewIconSize");
    toolsIconSize = smi->getValue("CoreApps", "ToolsIconSize");

    // get app's settings
    windowSize = smi->getValue("CoreShot", "WindowSize");
    windowMaximized = smi->getValue("CoreShot", "WindowMaximized");
    saveLocation = QString(smi->getValue("CoreShot", "SaveLocation"));
}

void coreshot::setPixmap(QPixmap &pix)
{
    ui->shotPreview->setOriginalPixmap(pix);
}

void coreshot::on_save_clicked()
{
    QString fileName = saveLocation;
    if (!CPrime::FileUtils::isDir(fileName)) {
        qDebug() << "func(on_save_clicked) : Error : Invalid saving directory :" << fileName;
        qDebug() << "creating " << saveLocation;
        QDir().mkpath(saveLocation);
    }

    // Remove the last '/' if exists
    fileName = QDir(fileName).path();

    if (fileName.length()) {
        QFile file(fileName + "/Screenshot_" + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss") + ".png");
        ui->shotPreview->originalPixmap().save(&file, "PNG");
        file.close();
        files = fileName;

        // Save the save location for future auto save feature
        smi->setValue("CoreShot", "SaveLocation", CPrime::FileUtils::dirName( fileName ));

        // Function from utilities.cpp
		CPrime::MessageEngine::showMessage(
					"cc.cubocore.CoreShot",
					"CoreShot",
					"Screenshot is saved",
					"You can see the file on\n" + saveLocation
		);

        this->hide();
        // Based on Message engine time how it will stay
        QTimer::singleShot(1000, this, SLOT(close()));
    } else {
		CPrime::MessageEngine::showMessage(
					"cc.cubocore.CoreShot",
					"CoreShot",
					"Screenshot is not saved",
					"Please select a valid directory from CoreGarage."
		);
        return;
    }
}

void coreshot::on_saveAs_clicked()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("Save Screenshot"), saveLocation, tr("Image (*.png)"));

    if(filename.isEmpty())
        return;

    QFile file(filename);

    ui->shotPreview->originalPixmap().save(&file, "PNG");

    file.close();
    files = filename;

    // Save the save location for future auto save feature
    smi->setValue("CoreShot", "SaveLocation", CPrime::FileUtils::dirName( filename ));

    // Function from utilities.cpp
	CPrime::MessageEngine::showMessage(
				"cc.cubocore.CoreShot",
				"CoreShot",
				"Screenshot Saved",
                "See the file on saved location."
	);

    this->hide();
    // Based on Message engine time how it will stay
    QTimer::singleShot(1000, this, SLOT(close()));
}

void coreshot::on_cancel_clicked()
{
    this->close();
}

void coreshot::on_newShot_clicked()
{
    coreshotdialog *shotD = new coreshotdialog();
    shotD->show();
    this->close();
}

void coreshot::on_openInEditor_clicked()
{
    QString fileName = saveLocation;
    if (!CPrime::FileUtils::isDir(fileName)) {
        qDebug() << "func(on_openInEditor_clicked) : Error : Invalid saving directory :" << fileName;
        qDebug() << "creating " << saveLocation;
        QDir().mkpath(saveLocation);
    }

    // Remove the last '/' if exists
    fileName = QDir(fileName).path();

    if (!fileName.length()) {
		CPrime::MessageEngine::showMessage(
					"cc.cubocore.CoreShot",
					"CoreShot",
					"Save directory does not exist",
					"Please select a valid directory from CoreGarage.\nOpening in editor needs to save the file first."
		);
        return;
    }

    fileName = saveLocation + "/Screenshot_" + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss") + ".png";
    QFile file(fileName);
    ui->shotPreview->originalPixmap().save(&file, "PNG");
    file.close();
    files = fileName;

    CPrime::AppOpenFunc::defaultAppEngine(CPrime::DefaultAppCategory::ImageEditor, QFileInfo(files), "");
}

void coreshot::on_shareIt_clicked()
{
    QString fileName = saveLocation;
    if (!CPrime::FileUtils::isDir(fileName)) {
        qDebug() << "func(on_openInEditor_clicked) : Error : Invalid saving directory :" << fileName;
        qDebug() << "creating " << saveLocation;
        QDir().mkpath(saveLocation);
    }

    // Remove the last '/' if exists
    fileName = QDir(fileName).path();

    if (!fileName.length()) {
		CPrime::MessageEngine::showMessage(
					"cc.cubocore.CoreShot",
					"CoreShot",
					"Save directory does not exist",
					"Please select a valid directory from CoreGarage.\nOpening in editor needs to save the file first."
		);
        return;
    }

    fileName = saveLocation + "/Screenshot_" + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss") + ".png";
    QFile file(fileName);
    ui->shotPreview->originalPixmap().save(&file, "PNG");
    file.close();
    files = fileName;

    if ( !files.isNull() ) {
        ShareIT *t = new ShareIT(QStringList() << files, listViewIconSize, nullptr);
        // Set ShareIT window size
        if (uiMode == 2 )
            t->setFixedSize(QGuiApplication::primaryScreen()->size() * .8 );
        else
            t->resize(500,600);

        t->exec();
    }
}

void coreshot::closeEvent(QCloseEvent *cEvent)
{
    qDebug()<< "save window stats"<< this->size() << this->isMaximized();

    smi->setValue("CoreShot", "WindowSize", this->size());
    smi->setValue("CoreShot", "WindowMaximized", this->isMaximized());

    cEvent->accept();
}
